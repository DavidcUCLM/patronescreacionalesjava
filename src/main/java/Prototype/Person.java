package Prototype;

interface Person {
    Person clone();
}