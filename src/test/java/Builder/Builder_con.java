package Builder;

import AbstractFactory.AbstractFactory;
import AbstractFactory.Architecture;
import AbstractFactory.CPU;
import junit.framework.TestCase;

import junit.framework.TestCase;

public class Builder_con extends TestCase {
	private static int inicio = 0;
	private static int repeticiones = 1000000;
	private static int prueba;
	
	public Builder_con(String sTestName)
	{
		super(sTestName);
	}

	public void setUp() throws Exception
	{
	}

	public void tearDown() throws Exception
	{
	}

	private void test() throws InterruptedException
	{
		
		
		System.out.println("inicio del test: abstract factory, p"+prueba+" ["+inicio+","+(inicio+repeticiones)+"]");
		for(int i=10 ;i>-1 ; i--) {
			Thread.sleep(1000);
			System.out.println("en: "+i+" seg");
		}
		

		for(int i=inicio; i< inicio+repeticiones ;i++) {
			System.out.println(i);
			
			 Waiter waiter = new Waiter();
		        PizzaBuilder hawaiianPizzabuilder = new HawaiianPizzaBuilder();
		        PizzaBuilder spicyPizzaBuilder = new SpicyPizzaBuilder();

		        waiter.setPizzaBuilder( hawaiianPizzabuilder );
		        waiter.constructPizza();

		        Pizza pizza = waiter.getPizza();
			
			
		}
				
		System.out.println("fin del test: abstract factory, p"+prueba+" ["+inicio+","+(inicio+repeticiones)+"]");
	}
	
	public void testNuevoAdmin() throws InterruptedException
	{
		for(prueba = 1; prueba<21 ;prueba++) {
			test();
			inicio += repeticiones;
		}
	}
	

}