package Prototype;


import junit.framework.TestCase;

public class Prototype_con extends TestCase {
	private static int inicio = 0;
	private static int repeticiones = 1000000;
	private static int prueba;
	
	public Prototype_con(String sTestName)
	{
		super(sTestName);
	}

	public void setUp() throws Exception
	{
	}

	public void tearDown() throws Exception
	{
	}

	private void test() throws InterruptedException
	{
		
		String[] args = null;
		System.out.println("inicio del test: abstract factory, p"+prueba+" ["+inicio+","+(inicio+repeticiones)+"]");
		for(int i=10 ;i>-1 ; i--) {
			Thread.sleep(1000);
			System.out.println("en: "+i+" seg");
		}
		

		for(int i=inicio; i< inicio+repeticiones ;i++) {
			System.out.println(i);
			
			 if (args.length > 0) {
		            for (String type : args) {
		                Person prototype = Factory.getPrototype(type);
		                if (prototype != null) {
		                    System.out.println(prototype);
		                }
		            }
		        } else {
		            System.out.println("Run again with arguments of command string ");
		        }
		}
				
		System.out.println("fin del test: abstract factory, p"+prueba+" ["+inicio+","+(inicio+repeticiones)+"]");
	}
	
	public void testNuevoAdmin() throws InterruptedException
	{
		for(prueba = 1; prueba<21 ;prueba++) {
			test();
			inicio += repeticiones;
		}
	}
	

}
